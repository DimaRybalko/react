import CardWrapper from "../components/card/CardWrapper";
import Card from "../components/card/Card";
import "./styles.css";
import Footer from "../components/footer/Footer.jsx";
import Button from "../components/Button.jsx";
import ModalTotal from "../components/Modal/ModalTotal.jsx";
import DeleteCardButton from "../components/card/DeleteCardButton.jsx";
import ModalDelete from "../components/Modal/ModalDelete.jsx";

function Cartpage({
  openSecond,
  secondActive,
  purchased,
  openDelete,
  firstActive,
  deleteCard,
  setCurrentItem,
}) {
  let totalCost = purchased.reduce(
    (acc, cost) => parseInt(acc) + parseInt(cost.price),
    0
  );

  return (
    <>
      <div className="additional_page">
        <h2 className="title additional_title">Your cart</h2>
        <CardWrapper>
          {purchased.map((item) => {
            const { name, price, color, articul, img, id, count } = item;

            return (
              <Card
                key={articul}
                img={img}
                name={name}
                price={price}
                color={color}
                articul={articul}
                id={id}
                count={count}
              >
                <DeleteCardButton
                  onClick={() => {
                    openDelete();
                    setCurrentItem(id);
                  }}
                />
                {/* К-сть замовлених товарів: <Counter /> */}
              </Card>
            );
          })}
        </CardWrapper>
        {secondActive && (
          <ModalTotal
            closeClick={openSecond}
            onClick={openSecond}
            tittle="Are you sure you want to buy all this products?"
          />
        )}
        {firstActive && (
          <ModalDelete
            purchased={purchased}
            deleteCard={deleteCard}
            closeClick={openDelete}
            onClick={openDelete}
            tittle="Are you sure you want to delete this product?"
          />
        )}
      </div>
      <div className="footer">
        <Footer totalCost={totalCost} />
        <Button onClick={openSecond} className="purchase_button">
          Buy
        </Button>
      </div>
    </>
  );
}

export default Cartpage;
