import "./styles.css";
import CardWrapper from "../components/card/CardWrapper";
import Card from "../components/card/Card";
import Button from "../components/Button";
import FavoriteButton from "../components/FavoriteButton";
import ModalText from "../components/Modal/ModalText";
function Favoritepage({
  openSecond,
  toggleFavorite,
  favs,
  currentItem,
  togglePurchase,
  secondActive,
}) {
  return (
    <>
      <div className="additional_page">
        <h2 className="title additional_title">Favorites</h2>
        <CardWrapper>
          {favs.map((item) => {
            const { name, price, color, articul, img, id } = item;
            return (
              <Card
                key={articul}
                img={img}
                name={name}
                price={price}
                color={color}
                articul={articul}
              >
                <Button
                  className="purchase_button"
                  onClick={() => {
                    openSecond(item);
                  }}
                >
                  Order
                </Button>

                <FavoriteButton
                  onClick={() => {
                    toggleFavorite(id, item);
                  }}
                  className={favs.includes(id) ? "" : "liked"}
                />
              </Card>
            );
          })}
        </CardWrapper>
        {secondActive && (
          <ModalText
            confirm={() => {
              togglePurchase(currentItem.id);
              console.log("id", currentItem.id);
            }}
            closeClick={openSecond}
            onClick={openSecond}
            tittle="Are you sure you want to add this product to cart?"
            articul={
              <div className="modal_card">
                <img className="modal_img" src={currentItem.img} alt="" />
                {currentItem.name}
                <br /> {currentItem.price}
              </div>
            }
          />
        )}
      </div>
    </>
  );
}
export default Favoritepage;
