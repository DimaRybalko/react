import { Route, Routes } from "react-router-dom";
import "./App.css";
import Homepage from "./pages/Homepage";
import Favoritepage from "./pages/Favoritepage";
import Cartpage from "./pages/Cartpage";
import { useState, useEffect } from "react";
import Header from "./components/header/header";
import { sendRequest } from "./Helpers/SendRequest";

function App() {
  const [secondActive, setsecondActive] = useState(false);
  const [firstActive, setfirstActive] = useState(false);
  const favorites = JSON.parse(localStorage.getItem("favs")) || [];
  const cart = JSON.parse(localStorage.getItem("purchased")) || [];
  const [favs, setFav] = useState(favorites);
  const [purchased, setPurchase] = useState(cart);
  const [currentItem, setCurrentItem] = useState({});
  const [products, setProducts] = useState([]);

  useEffect(() => {
    sendRequest("/products.json").then((result) => {
      setProducts(result);
    });
  }, []);
  useEffect(() => {
    const handleLocal = () => {
      localStorage.setItem("favs", JSON.stringify(favs));
      localStorage.setItem("purchased", JSON.stringify(purchased));
    };

    handleLocal();
  }, [favs, purchased]);
  function openDelete(item) {
    setfirstActive(!firstActive);
    setCurrentItem(item);
  }

  function openSecond(item) {
    setsecondActive(!secondActive);
    setCurrentItem(item);
  }

  function toggleFavorite(id) {
    setFav((prev) => {
      return prev.includes(id)
        ? prev.filter((number) => number !== id)
        : [...prev, id];
    });
  }

  function togglePurchase(id) {
    setPurchase((prev) => {
      return [...prev, id];
    });
  }

  function deleteCard() {
    setPurchase(() => {
      return purchased.filter((item) => {
        return currentItem !== item;
      });
    });
  }

  const newFavorites = products.filter((item) => favs.includes(item.id));
  const newCart = products.filter((item) => purchased.includes(item.id));

  return (
    <>
      <div className="header">
        <Header favs={favs.length} purchased={purchased.length} />
      </div>
      <Routes>
        <Route
          index
          element={
            <Homepage
              purchased={purchased}
              openSecond={openSecond}
              toggleFavorite={toggleFavorite}
              favs={favs}
              currentItem={currentItem}
              togglePurchase={togglePurchase}
              secondActive={secondActive}
              products={products}
            />
          }
        />
        <Route
          path="/favorites"
          element={
            <Favoritepage
              toggleFavorite={toggleFavorite}
              favs={newFavorites}
              currentItem={currentItem}
              togglePurchase={togglePurchase}
              secondActive={secondActive}
              openSecond={openSecond}
            />
          }
        />
        <Route
          path="/cart"
          element={
            <Cartpage
              deleteCard={deleteCard}
              openDelete={openDelete}
              purchased={newCart}
              openSecond={openSecond}
              secondActive={secondActive}
              firstActive={firstActive}
              setCurrentItem={setCurrentItem}
            />
          }
        />
      </Routes>
    </>
  );
}
export default App;
