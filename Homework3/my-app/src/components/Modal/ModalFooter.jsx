import Button from "../Button";

function ModalFooter({ firstText, secondaryText, firstClick, secondaryClick }) {
  return (
    <div className="buttons">
      {secondaryText && (
        <Button className="button" onClick={secondaryClick}>
          {secondaryText}
        </Button>
      )}
      {firstText && (
        <Button
          className="button first_button"
          onClick={() => {
            firstClick();
            secondaryClick();
          }}
        >
          {firstText}
        </Button>
      )}
    </div>
  );
}

export default ModalFooter;
