function ModalClose({ onClick }) {
  return (
    <div onClick={onClick} className="modal_close">
      <p>X</p>
    </div>
  );
}

export default ModalClose;
