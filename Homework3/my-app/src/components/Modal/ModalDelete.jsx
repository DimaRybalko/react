import ModalWrapper from "./ModalWrapper";
import ModalHeader from "./ModalHeader";
import ModaladditionalFooter from "./ModaladditionalFooter";
import ModalClose from "./ModalClose";
import ModalBody from "./ModalBody";
import Modal from "./Modal";

function ModalDelete({ tittle, onClick, closeClick, deleteCard }) {
  return (
    <ModalWrapper onClick={closeClick}>
      <Modal className="second_modal">
        <ModalHeader>
          <ModalClose onClick={onClick} />
        </ModalHeader>

        <ModalBody>
          <h2 className="modal_title">{tittle}</h2>
        </ModalBody>

        <ModaladditionalFooter
          secondaryText="No"
          firstText="Yes"
          tittle={tittle}
          secondaryClick={() => {
            closeClick();
            deleteCard();
          }}
        ></ModaladditionalFooter>
      </Modal>
    </ModalWrapper>
  );
}

export default ModalDelete;
