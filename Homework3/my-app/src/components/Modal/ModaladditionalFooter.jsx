import Button from "../Button";

function ModaladditionalFooter({ firstText, secondaryText, secondaryClick }) {
  return (
    <div className="buttons">
      {secondaryText && (
        <Button className="button" onClick={secondaryClick}>
          {secondaryText}
        </Button>
      )}
      {firstText && (
        <Button
          className="button first_button"
          onClick={() => {
            secondaryClick();
          }}
        >
          {firstText}
        </Button>
      )}
    </div>
  );
}

export default ModaladditionalFooter;
