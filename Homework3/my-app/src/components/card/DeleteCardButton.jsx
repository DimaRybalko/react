function DeleteCardButton({ onClick, id }) {
  return (
    <div onClick={onClick} id={id} className="delete_card">
      <p>X</p>
    </div>
  );
}

export default DeleteCardButton;
