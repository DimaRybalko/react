import "./footer.css";
function Footer({ totalCost }) {
  return (
    <div>
      <p className="title ">Total cost : {totalCost} $</p>
    </div>
  );
}

export default Footer;
