import { Link } from "react-router-dom";

function Logo() {
  return (
    <div className="logo_container">
      <Link to="/">
        <img src="./images/Logo.png" alt="" />
      </Link>
    </div>
  );
}
export default Logo;
