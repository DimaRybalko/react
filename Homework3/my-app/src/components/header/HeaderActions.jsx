import ShoppingCart from "./HeaderShoppingCart.jsx";
import Favorites from "./HeaderFavorite.jsx";
import { useState } from "react";
import { Link } from "react-router-dom";
function Actions({ favs, purchased }) {
  const [cartOpen, setCartOpen] = useState(false);
  function open() {
    setCartOpen(!cartOpen);
  }
  return (
    <div className="actions">
      <>
        <Link className="styled_link" to="/favorites">
          <Favorites favs={favs} />
        </Link>

        <Link className="styled_link" to="/cart">
          <ShoppingCart purchased={purchased} onClick={open} />
        </Link>

        {cartOpen && <div className="cart-menu"></div>}
      </>
    </div>
  );
}
export default Actions;
