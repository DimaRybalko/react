import "./App.css";
import Button from "./components/Button.jsx";
import { useState } from "react";
import ModalImage from "./components/Modal/ModalImage";
import ModalText from "./components/Modal/ModalText";

function FirstApp() {
  const [active, setActive] = useState(false);
  function openFirst() {
    setActive(!active);
  }
  const [secondActive, setsecondActive] = useState(false);
  function openSecond() {
    setsecondActive(!secondActive);
  }
  return (
    <>
      <div className="main_buttons">
        <Button className="main_button" onClick={openFirst}>
          {" "}
          Open first modal
        </Button>
        <Button className="main_button" onClick={openSecond}>
          {" "}
          Open second modal
        </Button>
      </div>
      {active && (
        <ModalImage
          closeClick={openFirst}
          onClick={openFirst}
          tittle="Product Delete!"
          desc="By clicking the “Yes, Delete” button, PRODUCT NAME will be deleted."
        />
      )}
      {secondActive && (
        <ModalText
          closeClick={openSecond}
          onClick={openSecond}
          tittle="Add Product 'NAME' "
          desc="description for your product"
        />
      )}
    </>
  );
}

export default FirstApp;
