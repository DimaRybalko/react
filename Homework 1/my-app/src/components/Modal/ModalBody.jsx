function ModalBody({ children }) {
  return <div className="modal_body">{children}</div>;
}

export default ModalBody;
