import ModalWrapper from "./ModalWrapper";
import ModalHeader from "./ModalHeader";
import ModalFooter from "./ModalFooter";
import ModalClose from "./ModalClose";
import ModalBody from "./ModalBody";
import Modal from "./Modal";

function ModalImage({ tittle, desc, onClick, closeClick }) {
  return (
    <ModalWrapper onClick={closeClick}>
      <Modal className="modal">
        <ModalHeader>
          <ModalClose onClick={onClick} />
        </ModalHeader>

        <ModalBody>
          <div className="body_img"></div>
          <h2 className="title">{tittle}</h2>
          <p className="description">{desc}</p>
        </ModalBody>

        <ModalFooter
          firstText="NO, CANCEL"
          secondaryText="YES, DELETE"
        ></ModalFooter>
      </Modal>
    </ModalWrapper>
  );
}

export default ModalImage;
