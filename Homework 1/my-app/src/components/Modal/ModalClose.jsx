function ModalClose({ onClick }) {
  return (
    <div onClick={onClick} className="modal_close">
      X
    </div>
  );
}

export default ModalClose;
