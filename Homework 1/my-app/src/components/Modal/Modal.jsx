import "./Modal.css";

function Modal({ children, className }) {
  const stopPropagation = (e) => {
    e.stopPropagation();
  };
  return (
    <div onClick={stopPropagation} className={className}>
      {children}
    </div>
  );
}

export default Modal;
