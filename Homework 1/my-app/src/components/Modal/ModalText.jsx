import ModalWrapper from "./ModalWrapper";
import ModalHeader from "./ModalHeader";
import ModalFooter from "./ModalFooter";
import ModalClose from "./ModalClose";
import ModalBody from "./ModalBody";
import Modal from "./Modal";

function ModalText({ tittle, desc, onClick, closeClick }) {
  return (
    <ModalWrapper onClick={closeClick}>
      <Modal className="second_modal">
        <ModalHeader>
          <ModalClose onClick={onClick} />
        </ModalHeader>

        <ModalBody>
          <h2 className="title">{tittle}</h2>
          <p className="description">{desc}</p>
        </ModalBody>

        <ModalFooter firstText="ADD TO FAVORITE"></ModalFooter>
      </Modal>
    </ModalWrapper>
  );
}

export default ModalText;
