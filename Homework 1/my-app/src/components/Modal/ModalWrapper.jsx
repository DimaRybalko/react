function ModalWrapper({ children, onClick }) {
  return (
    <div onClick={onClick} className="modalWrapper">
      {children}
    </div>
  );
}

export default ModalWrapper;
