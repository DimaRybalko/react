import Button from "../Button";

function ModalFooter({ firstText, secondaryText, firstClick, secondaryClick }) {
  return (
    <div className="buttons">
      {firstText && (
        <Button className="button first_button" onClick={firstClick}>
          {firstText}
        </Button>
      )}
      {secondaryText && (
        <Button className="button" onClick={secondaryClick}>
          {secondaryText}
        </Button>
      )}
    </div>
  );
}

export default ModalFooter;
