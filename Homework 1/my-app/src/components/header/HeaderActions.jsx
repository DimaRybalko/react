import ShoppingCart from "./HeaderShoppingCart.jsx";
import Favorites from "./HeaderFavorite.jsx";
import { useState } from "react";
function Actions({ likes, purchased }) {
  const [cartOpen, setCartOpen] = useState(false);
  function open() {
    setCartOpen(!cartOpen);
  }
  return (
    <div className="actions">
      <>
        <Favorites likes={likes} />
        <ShoppingCart
          purchased={purchased}
          onClick={open}
          className={cartOpen && "active_cart"}
        />
        {cartOpen && <div className="cart-menu"></div>}
      </>
    </div>
  );
}
export default Actions;
