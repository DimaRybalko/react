function Card(props) {
  return (
    <div className="card">
      <img className="card_img" src={props.img} alt="картинка" />
      <span className="card_name">{props.name}</span>
      <span className="card_color">{props.color}</span>
      <span className="card_articul">{props.articul}</span>
      <span className="card_price">{props.price}</span>
      <div className="buttons">{props.children}</div>
    </div>
  );
}

export default Card;
