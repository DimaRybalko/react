function CardWrapper({ children }) {
  return <div className="wrapper">{children}</div>;
}
export default CardWrapper;
