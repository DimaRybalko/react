import { useEffect, useState } from "react";
import "./App.css";
import Card from "./components/card/Card";
import CardWrapper from "./components/CardWrapper.jsx";
import Hero from "./components/Hero.jsx";
import HeroImg from "./components/HeroImg.jsx";

function App() {
  const [products, setProducts] = useState([]);
  useEffect(() => {
    async function fetching() {
      const response = await fetch("/products.json");
      const data = await response.json();
      setProducts(data);
    }
    fetching();
  }, []);

  return (
    <div className="main">
      <Hero>
        <h2 className="hero_text">
          Obsessive about design. <br />
          Always control your time.
          <br />
        </h2>
        <HeroImg />
      </Hero>
      <h2 className="title">All collection</h2>
      <CardWrapper>
        {products.map(({ name, price, color, articul, img }) => {
          return (
            <Card
              img={img}
              name={name}
              price={price}
              color={color}
              articul={articul}
            />
          );
        })}
      </CardWrapper>
    </div>
  );
}

export default App;
