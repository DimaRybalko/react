import "../pages/styles.css";
function Button({ type = "button", onClick, children, className }) {
  return (
    <button onClick={onClick} type={type} className={className}>
      {children}
    </button>
  );
}
export default Button;
