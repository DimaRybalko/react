import ShoppingCart from "./HeaderShoppingCart.jsx";
import Favorites from "./HeaderFavorite.jsx";
import { useState } from "react";
import { Link } from "react-router-dom";
import { useSelector } from "react-redux";
import { selectFavorites, selectBasket } from "../../store/selectors.js";
function Actions() {
  const [cartOpen, setCartOpen] = useState(false);
  const favs = useSelector(selectFavorites);
  const purchased = useSelector(selectBasket);

  function open() {
    setCartOpen(!cartOpen);
  }
  return (
    <div className="actions">
      <>
        <Link className="styled_link" to="/favorites">
          <Favorites favs={favs} />
        </Link>

        <Link className="styled_link" to="/cart">
          <ShoppingCart purchased={purchased} onClick={open} />
        </Link>

        {cartOpen && <div className="cart-menu"></div>}
      </>
    </div>
  );
}
export default Actions;
