import ModalWrapper from "./ModalWrapper";
import ModalHeader from "./ModalHeader";
import ModaladditionalFooter from "./ModaladditionalFooter";
import ModalClose from "./ModalClose";
import ModalBody from "./ModalBody";
import Modal from "./Modal";
import { useSelector, useDispatch } from "react-redux";
import { selectCurrentItem } from "../../store/selectors";
import { deleteFromBasket } from "../../store/actions.js";

function ModalDelete({ tittle, onClick, closeClick }) {
  const dispatch = useDispatch();
  const currentId = useSelector(selectCurrentItem);

  return (
    <ModalWrapper onClick={closeClick}>
      <Modal className="second_modal">
        <ModalHeader>
          <ModalClose onClick={onClick} />
        </ModalHeader>

        <ModalBody>
          <h2 className="modal_title">{tittle}</h2>
        </ModalBody>

        <ModaladditionalFooter
          secondaryText="No"
          firstText="Yes"
          tittle={tittle}
          secondaryClick={() => {
            closeClick();
          }}
          firstlyClick={() => {
            dispatch(deleteFromBasket(currentId.id));
            closeClick();
          }}
        ></ModaladditionalFooter>
      </Modal>
    </ModalWrapper>
  );
}

export default ModalDelete;
