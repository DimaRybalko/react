import Button from "../Button";

function ModaladditionalFooter({
  firstText,
  secondaryText,
  secondaryClick,
  firstlyClick,
}) {
  return (
    <div className="buttons">
      {secondaryText && (
        <Button
          className="button"
          onClick={() => {
            secondaryClick();
          }}
        >
          {secondaryText}
        </Button>
      )}
      {firstText && (
        <Button
          className="button first_button"
          onClick={() => {
            firstlyClick();
          }}
        >
          {firstText}
        </Button>
      )}
    </div>
  );
}

export default ModaladditionalFooter;
