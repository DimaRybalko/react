import ModalWrapper from "./ModalWrapper";
import ModalHeader from "./ModalHeader";
import ModalFooter from "./ModalFooter";
import ModalClose from "./ModalClose";
import ModalBody from "./ModalBody";
import Modal from "./Modal";

function ModalText({ tittle, onClick, closeClick, confirm, articul, img }) {
  return (
    <>
      <ModalWrapper onClick={closeClick}>
        <Modal className="second_modal">
          <ModalHeader>
            <ModalClose onClick={onClick} />
          </ModalHeader>

          <ModalBody>
            <>
              <h2 className="modal_title">{tittle}</h2>
              <img src={img} alt="" />
              <p>{articul}</p>
            </>
          </ModalBody>

          <ModalFooter
            firstClick={confirm}
            secondaryText="No"
            firstText="Yes"
            secondaryClick={() => {
              closeClick();
            }}
          ></ModalFooter>
        </Modal>
      </ModalWrapper>
    </>
  );
}

export default ModalText;
