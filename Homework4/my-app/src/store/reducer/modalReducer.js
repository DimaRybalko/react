import { createReducer } from "@reduxjs/toolkit";
import * as actions from "../actions.js";

const initialState = {
  currentItem: {},
  ModalStatus: false,
};

const modalReducer = createReducer(initialState, (builder) => {
  builder
    .addCase(actions.setCurrentItem, (state, { payload }) => {
      state.currentItem = payload;
    })
    .addCase(actions.actionFirstActive, (state, { payload }) => {
      state.ModalStatus = payload;
    })
    .addCase(actions.actionSecondActive, (state, { payload }) => {
      state.ModalStatus = payload;
    });
});

export default modalReducer;
