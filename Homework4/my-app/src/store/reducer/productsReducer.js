import { createReducer } from "@reduxjs/toolkit";
import * as actions from "../actions.js";
import { favoriteLocal, basketLocal } from "../actions.js";

const initialState = {
  products: [],
  favorites: [...favoriteLocal],
  basket: [...basketLocal],
};

const productsReducer = createReducer(initialState, (builder) => {
  builder
    .addCase(actions.addAllProducts, (state, { payload }) => {
      state.products = payload;
    })
    .addCase(actions.actionAddToFavorite, (state, { payload }) => {
      state.favorites = [...state.favorites, payload];
      localStorage.setItem("favorites", JSON.stringify(state.favorites));
    })
    .addCase(actions.actionDeleteFromFavorite, (state, { payload }) => {
      state.favorites = state.favorites.filter((number) => number !== payload);
      localStorage.setItem("favorites", JSON.stringify(state.favorites));
    })
    .addCase(actions.actionAddToCart, (state, { payload }) => {
      state.basket = [...state.basket, payload];
      localStorage.setItem("basket", JSON.stringify(state.basket));
    })
    .addCase(actions.actionDeleteFromCart, (state, { payload }) => {
      state.basket = state.basket.filter((item) => payload !== item);
      localStorage.setItem("basket", JSON.stringify(state.basket));
    });
});

export default productsReducer;
