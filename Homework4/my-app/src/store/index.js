import { configureStore } from "@reduxjs/toolkit";
import productsReducer from "./reducer/productsReducer.js";
import modalReducer from "./reducer/modalReducer.js";

export default configureStore({
  reducer: {
    products: productsReducer,
    modals: modalReducer,
  },
});
