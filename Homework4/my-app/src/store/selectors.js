export const selectFavorites = (state) => state.products.favorites;
export const selectCurrentItem = (state) => state.modals.currentItem;
export const selectProducts = (state) => state.products.products;
export const selectBasket = (state) => state.products.basket;
export const selectFirstModal = (state) => state.modals.ModalStatus;
export const selectSecondModal = (state) => state.modals.ModalStatus;
export const selectThirdModal = (state) => state.modals.ModalStatus;
