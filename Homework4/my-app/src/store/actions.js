import { createAction } from "@reduxjs/toolkit";
import { sendRequest } from "../Helpers/SendRequest";

export const actionAddToFavorite = createAction("ACTION_ADD_TO_FAVORITE");
export const actionDeleteFromFavorite = createAction(
  "ACTION_DELETE_FROM_FAVORITE"
);
export const setCurrentItem = createAction("SET_CURRENT_ITEM");
export const addAllProducts = createAction("ADD_ALL_PRODUCTS");
export const actionAddToCart = createAction("ACTION_ADD_TO_CART");
export const actionDeleteFromCart = createAction("ACTION_DELETE_FROM_CART");

export const actionFirstActive = createAction("ACTION_FIRST_ACTIVE");
export const actionSecondActive = createAction("ACTION_SECOND_ACTIVE");

export const toggleFavorite = (id) => (dispatch, getState) => {
  getState().products.favorites.includes(id)
    ? dispatch(actionDeleteFromFavorite(id))
    : dispatch(actionAddToFavorite(id));
};

export const fetchProducts = () => (dispatch) => {
  sendRequest("/products.json").then((result) => {
    dispatch(addAllProducts(result));
  });
};

export const addToBasket = (id) => (dispatch) => {
  dispatch(actionAddToCart(id));
};

export const deleteFromBasket = (id) => (dispatch) => {
  dispatch(actionDeleteFromCart(id));
};

export const currentItemId = (item) => (dispatch) => {
  dispatch(setCurrentItem(item));
};

export const openFirstModal = (item) => (dispatch) => {
  dispatch(actionFirstActive(item));
};

export const openSecondModal = (item) => (dispatch) => {
  dispatch(actionSecondActive(item));
};

export const favoriteLocal =
  JSON.parse(localStorage.getItem("favorites")) || [];
export const basketLocal = JSON.parse(localStorage.getItem("basket")) || [];
