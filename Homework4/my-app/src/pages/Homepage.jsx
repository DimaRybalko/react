import Button from "../components/Button.jsx";
import ModalText from "../components/Modal/ModalText.jsx";
import { useEffect } from "react";
import Card from "../components/card/Card.jsx";
import CardWrapper from "../components/card/CardWrapper.jsx";
import Hero from "../components/Hero/Hero.jsx";
import HeroImg from "../components/Hero/HeroImg.jsx";
import FavoriteButton from "../components/FavoriteButton.jsx";
import {
  toggleFavorite,
  addToBasket,
  fetchProducts,
} from "../store/actions.js";
import { useDispatch, useSelector } from "react-redux";
import {
  selectFavorites,
  selectProducts,
  selectSecondModal,
  selectCurrentItem,
} from "../store/selectors.js";
import { actionSecondActive } from "../store/actions.js";
import { setCurrentItem } from "../store/actions.js";

function Homepage() {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchProducts());
  }, []);
  const currentItem = useSelector(selectCurrentItem);
  const secondActive = useSelector(selectSecondModal);
  const products = useSelector(selectProducts);
  const favs = useSelector(selectFavorites);

  function openSecond(item) {
    dispatch(actionSecondActive(!secondActive));
    !!item && dispatch(setCurrentItem(item));
  }

  return (
    <>
      <div className="main">
        <Hero>
          <h2 className="hero_text">
            Obsessive about design. <br />
            Always control your time.
            <br />
          </h2>
          <HeroImg />
        </Hero>
        <h2 className="title">All collection</h2>
        <CardWrapper>
          {products.map((item) => {
            const { name, price, color, articul, img, id } = item;
            return (
              <Card
                key={articul}
                img={img}
                name={name}
                price={price}
                color={color}
                articul={articul}
              >
                <Button
                  className="purchase_button"
                  onClick={() => {
                    openSecond(item);
                  }}
                >
                  Order
                </Button>

                <FavoriteButton
                  onClick={() => {
                    dispatch(toggleFavorite(id));
                  }}
                  className={favs.includes(id) ? "liked" : ""}
                />
              </Card>
            );
          })}
        </CardWrapper>
        {secondActive && (
          <ModalText
            confirm={() => {
              dispatch(addToBasket(currentItem.id));
            }}
            closeClick={openSecond}
            onClick={openSecond}
            tittle="Are you sure you want to add this product to cart?"
            articul={
              <div className="modal_card">
                <img className="modal_img" src={currentItem.img} alt="" />
                {currentItem.name}
                <br /> {currentItem.price}
              </div>
            }
          />
        )}
      </div>
    </>
  );
}

export default Homepage;
