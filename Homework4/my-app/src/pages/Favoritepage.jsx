import "./styles.css";
import CardWrapper from "../components/card/CardWrapper";
import Card from "../components/card/Card";
import Button from "../components/Button";
import FavoriteButton from "../components/FavoriteButton";
import ModalText from "../components/Modal/ModalText";
import { toggleFavorite, addToBasket } from "../store/actions.js";
import { useDispatch } from "react-redux";
import { fetchProducts } from "../store/actions.js";
import { useEffect } from "react";
import { actionSecondActive } from "../store/actions.js";
import { setCurrentItem } from "../store/actions.js";
import { useSelector } from "react-redux";
import {
  selectSecondModal,
  selectFavorites,
  selectCurrentItem,
  selectProducts,
} from "../store/selectors.js";

function Favoritepage() {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(fetchProducts());
  }, []);
  const secondActive = useSelector(selectSecondModal);
  const favs = useSelector(selectFavorites);
  const currentItem = useSelector(selectCurrentItem);
  const products = useSelector(selectProducts);
  const newFavorites = products.filter((item) => favs.includes(item.id));

  function openSecond(item) {
    dispatch(actionSecondActive(!secondActive));
    !!item && dispatch(setCurrentItem(item));
  }
  return (
    <>
      <div className="additional_page">
        <h2 className="title additional_title">Favorites</h2>
        <CardWrapper>
          {newFavorites.map((item) => {
            const { name, price, color, articul, img, id } = item;
            return (
              <Card
                key={articul}
                img={img}
                name={name}
                price={price}
                color={color}
                articul={articul}
              >
                <Button
                  className="purchase_button"
                  onClick={() => {
                    openSecond(item);
                  }}
                >
                  Order
                </Button>

                <FavoriteButton
                  onClick={() => {
                    dispatch(toggleFavorite(id));
                  }}
                  className={newFavorites.includes(id) ? "" : "liked"}
                />
              </Card>
            );
          })}
        </CardWrapper>
        {secondActive && (
          <ModalText
            confirm={() => {
              dispatch(addToBasket(currentItem.id));
            }}
            closeClick={openSecond}
            onClick={openSecond}
            tittle="Are you sure you want to add this product to cart?"
            articul={
              <div className="modal_card">
                <img className="modal_img" src={currentItem.img} alt="" />
                {currentItem.name}
                <br /> {currentItem.price}
              </div>
            }
          />
        )}
      </div>
    </>
  );
}
export default Favoritepage;
