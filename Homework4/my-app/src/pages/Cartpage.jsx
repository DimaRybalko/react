import CardWrapper from "../components/card/CardWrapper";
import Card from "../components/card/Card";
import "./styles.css";
import Footer from "../components/footer/Footer.jsx";
import Button from "../components/Button.jsx";
import {
  selectFirstModal,
  selectProducts,
  selectBasket,
} from "../store/selectors.js";
import DeleteCardButton from "../components/card/DeleteCardButton.jsx";
import ModalDelete from "../components/Modal/ModalDelete.jsx";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { fetchProducts, setCurrentItem } from "../store/actions.js";
import { actionFirstActive } from "../store/actions.js";

function Cartpage() {
  const dispatch = useDispatch();
  const firstActive = useSelector(selectFirstModal);
  const products = useSelector(selectProducts);
  const purchased = useSelector(selectBasket);

  const newCart = products.filter((item) => purchased.includes(item.id));
  let totalCost = purchased.reduce(
    (acc, cost) => parseInt(acc) + parseInt(cost.price),
    0
  );
  useEffect(() => {
    dispatch(fetchProducts());
  }, []);

  function openDelete(item) {
    dispatch(actionFirstActive(!firstActive));
    !!item && dispatch(setCurrentItem(item));
  }

  return (
    <>
      <div className="additional_page">
        <h2 className="title additional_title">Your cart</h2>
        <CardWrapper>
          {newCart.map((item) => {
            const { name, price, color, articul, img, id, count } = item;

            return (
              <Card
                key={articul}
                img={img}
                name={name}
                price={price}
                color={color}
                articul={articul}
                id={id}
                count={count}
              >
                <DeleteCardButton
                  onClick={() => {
                    dispatch(setCurrentItem(item));
                    openDelete();
                  }}
                />
                {/* К-сть замовлених товарів: <Counter /> */}
              </Card>
            );
          })}
        </CardWrapper>
        {firstActive && (
          <ModalDelete
            purchased={purchased}
            closeClick={openDelete}
            onClick={openDelete}
            tittle="Are you sure you want to delete this product?"
          />
        )}
      </div>
      <div className="footer">
        <Footer totalCost={totalCost} />
        <Button className="purchase_button">Buy</Button>
      </div>
    </>
  );
}

export default Cartpage;
