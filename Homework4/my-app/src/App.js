import { Route, Routes } from "react-router-dom";
import "./App.css";
import Homepage from "./pages/Homepage";
import Favoritepage from "./pages/Favoritepage";
import Cartpage from "./pages/Cartpage";
import Header from "./components/header/header";

function App() {
  return (
    <>
      <div className="header">
        <Header />
      </div>
      <Routes>
        <Route index element={<Homepage />} />
        <Route path="/favorites" element={<Favoritepage />} />
        <Route path="/cart" element={<Cartpage />} />
      </Routes>
    </>
  );
}
export default App;
