import Actions from "./HeaderActions";
import Navigation from "./HeaderNavigation.jsx";
import Logo from "./HeaderLogo.jsx";
import HeaderInput from "./HeaderInput.jsx";
function Header({ likes, purchased }) {
  return (
    <header>
      <div className="header_container">
        <nav className="navigation">
          <Logo />
          <Navigation />
          <HeaderInput />
          <Actions likes={likes} purchased={purchased} />
        </nav>
      </div>
    </header>
  );
}
export default Header;
