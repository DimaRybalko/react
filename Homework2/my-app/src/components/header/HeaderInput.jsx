function HeaderInput() {
  return (
    <div className="input_box">
      <input className="input" type="text" placeholder="Search..." />
    </div>
  );
}

export default HeaderInput;
