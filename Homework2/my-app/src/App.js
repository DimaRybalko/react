import "./App.css";
import Button from "./components/Button.jsx";
import ModalText from "./components/Modal/ModalText";
import { useEffect, useState } from "react";
import Card from "./components/card/Card";
import CardWrapper from "./components/card/CardWrapper.jsx";
import Hero from "./components/Hero/Hero.jsx";
import HeroImg from "./components/Hero/HeroImg.jsx";
import FavoriteButton from "./components/FavoriteButton.jsx";
import Header from "./components/header/header.jsx";

function FirstApp() {
  const [secondActive, setsecondActive] = useState(false);
  const [products, setProducts] = useState([]);
  const favorites = JSON.parse(localStorage.getItem("favs")) || [];
  const cart = JSON.parse(localStorage.getItem("purchase")) || [];
  const [favs, setFav] = useState(favorites);
  const [purchase, setPurchase] = useState(cart);
  const [currentItem, setCurrentItem] = useState({});

  useEffect(() => {
    const handleLocal = () => {
      localStorage.setItem("favs", JSON.stringify(favs));
      localStorage.setItem("purchase", JSON.stringify(purchase));
    };

    handleLocal();
  }, [favs, purchase]);

  function openSecond(item) {
    setsecondActive(!secondActive);
    setCurrentItem(item);
  }
  useEffect(() => {
    async function fetching() {
      const response = await fetch("/products.json");
      const data = await response.json();
      setProducts(data);
    }
    fetching();
  }, []);
  function toggleFavorite(id) {
    setFav((prev) => {
      return prev.includes(id)
        ? prev.filter((number) => number !== id)
        : [...prev, id];
    });
  }

  function togglePurchase(id) {
    setPurchase((prev) => {
      return [...prev, id];
    });
  }

  console.log("currentItem", currentItem);
  return (
    <>
      <div className="header">
        <Header likes={favs.length} purchased={purchase.length} />
      </div>
      <div className="main">
        <Hero>
          <h2 className="hero_text">
            Obsessive about design. <br />
            Always control your time.
            <br />
          </h2>
          <HeroImg />
        </Hero>
        <h2 className="title">All collection</h2>
        <CardWrapper>
          {products.map((item) => {
            const { name, price, color, articul, img, id } = item;
            return (
              <Card
                key={articul}
                img={img}
                name={name}
                price={price}
                color={color}
                articul={articul}
              >
                <Button
                  className="purchase_button"
                  onClick={() => {
                    openSecond(item); /// викликаємо функцию та передаємо весь обькт картки
                  }}
                >
                  Order
                </Button>

                <FavoriteButton
                  onClick={() => {
                    toggleFavorite(id);
                  }}
                  className={favs.includes(id) ? "liked" : ""}
                />
              </Card>
            );
          })}
        </CardWrapper>
        {secondActive && (
          <ModalText
            confirm={() => {
              togglePurchase(currentItem.id);
            }}
            closeClick={openSecond}
            onClick={openSecond}
            tittle="Are you sure you want to add this product to cart?"
            articul={
              <div className="modal_card">
                <img className="modal_img" src={currentItem.img} alt="" />
                {currentItem.name}
                <br /> {currentItem.price}
              </div>
            }
          />
        )}
        <div className="footer"></div>
      </div>
    </>
  );
}

export default FirstApp;
